package com.amazing.earth.picture.app.raznoe

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amazing.earth.picture.app.R
import com.amazing.earth.picture.app.funs.BackIMG
import com.amazing.earth.picture.app.ui.theme.Black
import com.amazing.earth.picture.app.ui.theme.White

@Composable
fun Places(vernutsyaNazad:()->Unit) {


    val forpadding = LocalConfiguration.current.screenHeightDp
    val zalupenka = LocalConfiguration.current.screenWidthDp

    val places = remember{
        mutableListOf(
            R.drawable.place1,
            R.drawable.place2,
            R.drawable.place3,
            R.drawable.place4,
            R.drawable.place5,
            R.drawable.place6,
            R.drawable.place7,
            R.drawable.place8,
            R.drawable.place9,
            R.drawable.place10,
            R.drawable.place11,
            R.drawable.place12,
            R.drawable.place13,
            R.drawable.place14,
            R.drawable.place15,
            R.drawable.place16,
            R.drawable.place17,
            R.drawable.place18,
            R.drawable.place19,
            R.drawable.place20)
    }

    val description = remember{
        mutableListOf(
            "1. Bagan, Myanmar\n\nBagan is the capital of the first state in Myanmar. During its heyday, more than 10 thousand religious " +
                    "buildings were built on the local plains; today, in the abandoned temple complex there are more than 2 thousand ancient Buddhist temples." +
                    " Ancient pagodas, temples, sacred caves and stupas, located in the bend of the Irrawaddy River, preserve the memory of the former capital of the" +
                    " Burmese Empire. Bagan is an amazing and eclectic sight: the temples are built from limestone, sandstone, marble, with tiles and underground galleries" +
                    " - and each is at least somewhat different from its neighbors.",
            "2. Colored rocks of Zhangye Danxia in Gansu Province, China\n\nZhangye Danxia Colored Rocks are colorful rock formations in the " +
                    "Geological Park in Gansu Province, China. These formations represent a unique type of petrographic geomorphology and are composed of Cretaceous red" +
                    " sandstones and conglomerates.\n Today, the Zhangye Danxia Colored Cliffs Scenic Area is popular with travelers from all " +
                    "over the planet, who come here to enjoy scenic walks and boat rides through the area. which are also home to a number of temples.",
            "3. Li River, China\n\nThe Li River is one of the most picturesque places in China. National Geographic magazine included the River Lee in" +
                    " its list of ten water wonders of the world. The Li River is often called the “pearl of China”, and residents of the city of Guilin," +
                    " which is located on the banks of the river, consider it the soul and heart of their city. The river passes through the world's largest " +
                    "and most beautiful karst landscape. The most picturesque part of the river is an 83-kilometer section located between the cities of Guilin" +
                    " and Yangshuo. The landscape is adorned with green hills and steep cliffs that are full of fantastic caves.",
            "4. Monastic complex of Meteora, Greece\n\nMeteora is a famous monastery complex that occupies a vast area near Kalambaka," +
                    " which is located in northern Greece. What is unique is that the buildings are located on the giant rocks of Thessaly - " +
                    "they are called “meteoros”, which is translated from Greek as “hanging in the air”. The territory has been known as a place " +
                    "of hermitage since the eleventh century - the rich history of centuries attracts pilgrims and travelers from all over the world.",
            "5. Salar de Uyuni - the world's largest salt flat, Bolivia\n\nThe Salar de Uyuni, or Uyuni Salt Desert, is a dry body of water and the largest salt " +
                    "flat in the world. Once upon a time, the Salar de Uyuni was an integral part of the prehistoric salt lake that occupied much of southwestern" +
                    " Bolivia. After the reservoir dried up, it turned into several small seasonal lakes and salt marshes. The unparalleled beauty and atmosphere of" +
                    " this salt desert makes it one of the most impressive natural attractions in all of South America.",
            "6. Tianji Mountains, China\n\nEveryone has seen landscapes like this in movies like Avatar. Such mountains are typical for the fantasy style, where the action takes " +
                    "place on an unknown, distant planet. But the Tianji Mountains are absolutely real and are located in the Hunan province of China. Huge marble spiers " +
                    "soar hundreds of meters into the air from the base. Visitors can take the cable car to see all these wonders up close.",
            "7. Santorini Island, Greece\n\nSantorini is a magical island located in the Aegean Sea, in the very south of the Cyclades Islands. " +
                    "Santorini is more than 3,500 years old, and traces of ancient civilizations constantly remind us of the rich past of the archipelago. " +
                    "But here the last thing you want to think about is time: relaxation surrounded by volcanic landscapes, amazingly blue seas, well-groomed" +
                    " beaches with colorful sand and postcard houses with snow-white facades is truly serene. People come here to admire the sunsets, enjoy the moment," +
                    " and regain inner harmony lost in the eternal rush and bustle.",
            "8. Giant Angkor Temple Complex, Cambodia\n\nAngkor Wat, dedicated to the Hindu god Vishnu, is the largest religious building in the world. This is " +
                    "a complex three-level structure with many stairs and passages, topped with 5 towers. It is not for nothing that the temple is called the" +
                    " soul of the Khmer people, because Angkor exactly represents Khmer art, a great civilization, the heart of the nation. In total, the territory" +
                    " covers more than 400 sq. km there are about 200 monuments.",
            "9. Yosemite Valley, USA\n\nYosemite National Park is located on the western slopes of the Sierra Nevada range in California. This ridge " +
                    "stretches across all of California, its height increases from west to east. Yosemite Valley occupies only one percent of the park's " +
                    "total area, but it is where most visitors flock. One of the most popular sites in the world among rock climbers with routes of varying " +
                    "difficulty levels is the granite rock El Capitan, 2307 m above sea level, clearly visible from any part of the valley.",
            "10. Hitachi National Seaside Park, Japan\n\nHitachi National Park is an amazing example of landscape design. Flower fields" +
                    " stretching beyond the horizon are located on the territory of a former American military base. In the place where armed " +
                    "troops were once based, tulips, poppies, daffodils, forget-me-nots, lilies and sakura are now blooming in lush colors. A" +
                    " feature of the huge park, spread over an area of 120 hectares, is its adherence to the natural seasons: spring, summer, autumn " +
                    "and winter have their own floral covers.",
            "11. Sagano Bamboo Forest in Kyoto, Japan\n\nThe Sagano Bamboo Forest is an amazing bamboo grove containing thousands " +
                    "of thin and tall bamboo trees. The forest is one of Japan's most scenic natural attractions, and it is located in " +
                    "the western part of the city of Kyoto in a park called Arashiyama. The forest has become famous not only for its unsurpassed " +
                    "beauty, but also for the unique sounds that arise when wind currents come into contact with thin bamboo stems. Many visitors to the park" +
                    " come to this place to enjoy the special natural melodies.",
            "12. Ancient city of Petra, Jordan\n\nThe ancient city of Petra is rightfully considered the main attraction of Jordan, which made this eastern country famous throughout" +
                    " the globe, and one of the 7 new wonders of the world! They say that in the whole world there is nothing like Petra. Carved into the stone walls of the mountains, " +
                    "the pink-red city is full of mysterious charm, as if created to instill a piece of wonder in everyone who dares to visit it.",
            "13. Inca city Machu Picchu, Peru\n\nThe walled city of Machu Picchu is one of the most beautiful places on the planet, awarded the title of New Wonder of " +
                    "the World in 2007. Machu Picchu has many honorary titles - it is often called the “city in the sky” or “the city among the clouds”, sometimes – the " +
                    "“lost city of the Incas”. The mysterious city, invisible from below and completely autonomous in the past, is today considered the most important " +
                    "tourist attraction in Peru.",
            "14. Benagil Cave in Algarve, Portugal\n\nThe Algarve de Benagil cave is located in Portugal and" +
                    " is one of the most beautiful places not only in this country, but on the entire planet. " +
                    "This natural landmark was formed under the influence of water and wind, forming a natural" +
                    " cavity of incredible beauty. The cave is located next to the popular Benagil beach and can only be accessed from the sea.",
            "15. Ha Long Bay, Vietnam\n\nMajestic and mysterious, inspiring and amazing: all words are not enough to describe the beauty of this natural wonder - " +
                    "Halong Bay. Try to imagine more than three thousand islets rising from the emerald waters of the Gulf of Tonkin - and your imagination will " +
                    "not be half as beautiful as the views of the bay, which will take your breath away. Halong is a perfect work of art, a priceless collection" +
                    " of unfinished sculptures created by the hands of nature itself.",
            "16. Pamukkale, Türkiye\n\nThe thermal springs of Pamukkale are an incredible place created by nature itself and attracting millions " +
                    "of people every year. The name translates from Turkish as “cotton castle”. Indeed, when you look at the Pamukkale resort from a" +
                    " distance or in a photo, it seems that you see in front of you a large castle built from many fluffy cotton bolls. This place is" +
                    " also called “Cleopatra’s pool” - according to legend, it was here that the Egyptian queen drew beauty and youth.",
            "17. Rice terraces in Mu Can Chai, Vietnam\n\nThe rice terraces in Vietnam are considered one of the wonders of the world, created jointly by nature and man." +
                    " It seems that the best landscape designers in the world worked on them.",
            "18. Monument Valley, USA\n\nOn the border with the state of Arizona and in the southeast of the state of Utah there is one of " +
                    "the national symbols and simply the pride of the United States, a legendary place where hundreds of cowboy advertisements, " +
                    "videos, westerns and other films were filmed - Monument Valley. Since ancient times, the indigenous people of the United States, " +
                    "the Navajo Indian tribes, have lived in the valley in their huts without access to basic running water or electricity." +
                    " A visit to their settlements is part of the excursion, which will be kindly, but not free, provided to you by the monopolists in this matter - the Indians. " +
                    "They have established themselves as masters of the area and conduct daily tours for the huge number of tourists visiting the park.",
            "19. Perito Moreno Glacier in Patagonia, Argentina\n\nPerito Moreno is the jewel of the Los Glaciares National Park. This pile of huge blue ice floes is located" +
                    " on the border of the Argentine province of Santa Cruz and Chile. This fantastic natural monument in the Argentine part of Patagonia is just one of 48" +
                    " glaciers fed by the southern part of the glacier located in the Andes. If you're lucky, in Perito Moreno you can see how the huge ice dam of Lake Argentino " +
                    "breaks through, and ice floes accumulate on the opposite shore. This happens from once a year to once every ten years.",
            "20. Hang Son Doong Cave, Vietnam\n\nSon Doong is the largest cave on the planet and the main symbol of the Phong Nha-Kebang National Park, " +
                    "located 500 km south of Hanoi. Despite the honorable status, residents of the surrounding villages learned about the giant cave only in 1991, " +
                    "and the rest of the world in general in 2009, after an expedition of speleologists from the UK. So it is not surprising that its underground passages " +
                    "are still only in the initial stages of exploration.",
        )
    }
    val placeIndex = remember{
        mutableStateOf(0)
    }
    Box(modifier = Modifier.fillMaxSize())
    {
        BackIMG(image = R.drawable.placesback)

        Image(painter = painterResource(id = R.drawable.strelkanazad), contentDescription ="",
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding(start = (zalupenka * 0.02).dp, top = (forpadding * 0.042).dp)
                .clickable(MutableInteractionSource(), null) {
                    if (placeIndex.value != 0) {
                        placeIndex.value--
                    } else {
                        vernutsyaNazad.invoke()
                    }
                })
        Column (modifier = Modifier
            .fillMaxWidth()
            .align(Alignment.Center)
            .height((forpadding*0.7).dp),
            horizontalAlignment = Alignment.CenterHorizontally){
            Image(painter = painterResource(id = places[placeIndex.value]), contentDescription ="",modifier = Modifier.fillMaxWidth(), contentScale = ContentScale.FillBounds )
            LazyColumn{
                item{
                    Box()
                    {
                        Text(text = description[placeIndex.value], fontSize = 20.sp,
                            fontFamily = FontFamily(Font(R.font.topfont)),
                            color = Black, modifier = Modifier.offset(x = 1.dp, y = 1.dp))
                        Text(text = description[placeIndex.value], fontSize = 20.sp,
                            fontFamily = FontFamily(Font(R.font.topfont)),
                            color = Black, modifier = Modifier.offset(x = (-1).dp, y = (-1).dp))
                        Text(text = description[placeIndex.value], fontSize = 20.sp,
                            fontFamily = FontFamily(Font(R.font.topfont)),
                            color = White, modifier = Modifier)
                    }
                }
            }

        }

        if(placeIndex.value!=19){
            Image(painter = painterResource(id = R.drawable.nextbutton), contentDescription ="",
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = (forpadding * 0.05).dp)
                    .clickable(MutableInteractionSource(), null) {
                        placeIndex.value++
                    })
        }
        else{
            Image(painter = painterResource(id = R.drawable.gmenbuttom), contentDescription ="",
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = (forpadding * 0.05).dp)
                    .clickable(MutableInteractionSource(), null) {
                        vernutsyaNazad.invoke()
                    })
        }

    }
    BackHandler {
        if (placeIndex.value != 0) {
            placeIndex.value--
        } else {
            vernutsyaNazad.invoke()
        }
    }
}