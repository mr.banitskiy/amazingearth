package com.amazing.earth.picture.app.raznoe

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import com.amazing.earth.picture.app.R
import com.amazing.earth.picture.app.funs.BackIMG
import kotlinx.coroutines.delay

@Composable
fun Zagruzochka(poitiVMenu:()->Unit) {

    LaunchedEffect(Unit )
    {
        delay(3000)
        poitiVMenu.invoke()
    }
    Box(modifier = Modifier.fillMaxSize())
    {
        BackIMG(image = R.drawable.lod)
    }
}