package com.amazing.earth.picture.app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.amazing.earth.picture.app.funs.perehod
import com.amazing.earth.picture.app.raznoe.Menushka
import com.amazing.earth.picture.app.raznoe.Places
import com.amazing.earth.picture.app.raznoe.Zagruzochka
import com.amazing.earth.picture.app.ui.theme.AmazingEarthTheme

class AmazingPlacesActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.getInsetsController(window,window.decorView).apply {
            systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            hide(WindowInsetsCompat.Type.statusBars())
        }
        setContent {
            AmazingEarthTheme {

                val perehod = rememberNavController()

                NavHost(navController = perehod, startDestination = perehod(1) )
                {
                    composable(perehod(1))
                    {
                        Zagruzochka(poitiVMenu = {perehod.navigate(perehod(2))})
                    }
                    composable(perehod(2))
                    {
                        Menushka(goToPlaces = {perehod.navigate(perehod(3))})
                    }
                    composable(perehod(3))
                    {
                        Places(vernutsyaNazad = {perehod.navigate(perehod(2))})
                    }
                }
            }
        }
    }
}