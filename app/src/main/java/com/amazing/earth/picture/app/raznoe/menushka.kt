package com.amazing.earth.picture.app.raznoe

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.amazing.earth.picture.app.R
import com.amazing.earth.picture.app.funs.BackIMG
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun Menushka(goToPlaces:()->Unit) {

    val huet = LocalConfiguration.current.screenHeightDp


    val effect1 = remember{
        mutableStateOf(1f)
    }
    val effect2 = remember{
        mutableStateOf(1f)
    }

    val cska = CoroutineScope(Dispatchers.Main)

    val govnina = LocalContext.current as Activity

    Box(modifier = Modifier.fillMaxSize())
    {
        BackIMG(image = R.drawable.menushka)

        Column(modifier = Modifier
            .align(Alignment.Center)
            .padding(top = (huet * 0.15).dp),
            verticalArrangement = Arrangement.spacedBy((huet*0.1).dp)) {

            Image(painter = painterResource(id = R.drawable.startbutton),
                contentDescription ="",
                modifier = Modifier
                    .scale(effect1.value)
                    .clickable(MutableInteractionSource(), null) {
                        cska.launch {
                            effect1.value = 0.5f
                            delay(200)
                            effect1.value = 1f
                            goToPlaces.invoke()
                        }
                    })

            Image(painter = painterResource(id = R.drawable.exitbutton),
                contentDescription ="",
                modifier = Modifier
                    .scale(effect2.value)
                    .clickable(MutableInteractionSource(), null) {
                        cska.launch {
                            effect2.value = 0.5f
                            delay(200)
                            effect2.value = 1f
                            govnina.finish()
                        }
                    })
        }
    }

    BackHandler {
        govnina.finish()
    }

}